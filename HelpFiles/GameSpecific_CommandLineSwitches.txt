<code>-game <i>GAME_NAME</i></code> - Specify game to select in the Crew Chief

Example: <code>[full path]\CrewChiefV4.exe -game RACE_ROOM</code>
this will make RaceRoom the selected game in the Crew Chief.

Supported values:
&#8226; ACC
&#8226; AMS
&#8226; AMS2, AMS2_NETWORK
&#8226; ASSETTO_64BIT, ASSETTO_32BIT
&#8226; F1_2018, F1_2019
&#8226; FTRUCK
&#8226; GSC
&#8226; IRACING
&#8226; MARCAS
&#8226; PCARS2, PCARS2_NETWORK
&#8226; PCARS_64BIT, PCARS_32BIT, PCARS_NETWORK
&#8226; RACE_ROOM
&#8226; RF1
&#8226; RF2, RF2_64BIT <i>Note: 32 bit rFactor 2 is no longer supported. RF2 is the same as RF2_64BIT</i>


This can be used in conjunction with the launch_pcars / launch_raceroom / [game]_launch_exe / [game]_launch_params and run_immediately options to set crew chief up to start the game selected in the app launch argument, and start its own process.

<code>-profile <i>profile file name</i></code>- You can specify name of the profile to run at CC startup

Example: <code>[full path]\CrewChiefV4.exe -profile "my favorite game my awesome profile"</code>
this will load <code>"my favorite game my awesome profile.json"</code> profile at Crew Chief startup.


<code>-cpu<i>[1-8]</i></code> - You can set the processor affinity for Crew Chief in TaskManager, but this will have to be done each time you start the app. Alternatively, you can start the app with an addition argument "-cpu1", "-cpu2", ... "-cpu8", like this:

Example: <code>[full path]\CrewChiefV4.exe -cpu4</code>
this will set the processor affinity to the 4th CPU in your system (usually referred to as CPU3 - they're zero-indexed).


<code>-c_exit</code> - Pass this switch to close running Crew Chief instance.


<code>-nodevicescan</code> - Disable automatic active/disabled controller detection.  Use this if you have issues with CC rescanning controllers all the time (caused by buggy device drivers).


<code>-sound_test</code> - Enables extra UI that helps sound pack creators testing sounds.


<code>-skip_updates</code> - disables the check for CC updates.


<code>-debug</code> - collects CC debug trace.  For more info see <a href="http://thecrewchief.org/showthread.php?142-How-to-collect-Crew-Chief-repro-traces">here</a>

