There are two voice recognition systems which Crew Chief can use. The first of these is optimised for noisy environments and poor quality sound input. This is the default option. It requires no training and works with a wide range of microphone types and accents, but it requires a separate runtime installation and a language pack. This system is referred to as the "Microsoft speech recognition engine". The other system uses the speech recognition system built in to Windows. This requires a better quality voice input signal than the default system but can be trained to recognise a user's individual voice. It does not require the installation of any additional components. This system is referred to as the "Windows speech recognition engine".

Which is best depends on the quality of your microphone, your voice, the amount of background noise, and countless other factors. It's worth trying both and seeing which gives the best results.

If you want to use the Windows speech recognition engine simply ensure that the 'Prefer Windows speech recogniser' option is checked on the Properties screen. You will get better results if you work through the speech recognition training process in Windows.

If you want to use the default Microsoft voice recognition system, you'll need to install <a href="https://www.microsoft.com/en-gb/download/details.aspx?id=27225">the Microsoft Speech Recognition runtime</a>
From this page click 'Download' and select the right version - for most users this will be x64_SpeechPlatformRuntime\SpeechPlatformRuntime.msi
Once you've installed this, you'll also need to install <a href="https://www.microsoft.com/en-gb/download/details.aspx?id=27224">a language pack</a>
Again, from this page click 'Download' and select the right version - for most users this will be MSSpeech_SR_en-US_TELE.msi (US users) or MSSpeech_SR_en-GB_TELE.msi (UK users).

Note that the app will fall back to using the Windows speech recognition engine if it can't find a working installation of the Microsoft speech recognition engine.

Crew Chief will use the "Default" recording device for voice recognition - this can be selected in the "Recording devices" section of the Window Sounds control panel.

Voice recognition can be configured to be always-on (it listens continuously and responds when it recognises a command) or in response to a button press. To assign a button to a activate voice recognition, press the 'Scan for controllers' button at the bottom of Crew Chief UI. This will populate the 'Available controllers' list. Then select the controller you want to use from this list and select the 'Talk to crew chief' item from the 'Available actions' list. Then click the 'Assign control' button and press the wheel / controller / keyboard button you want to use for this action. By default you have to hold this button down while you talk - this can be changed by selecting a different 'Voice recognition mode'.

<h4>Testing voice recognition</h4>
Having set it all up press the <b>Start Application</b> button in Crew Chief, then press your voice recognition button and ask "Can you hear me?". If the app can understand you it'll respond with "Yes, I can hear you".

Make sure you test this while the game is running as some games claim exclusive control, so if no app is running that is claiming control it will always work.

If it doesn't work: right click on the speaker on the taskbar, select Open Sound Settings then Sound Control Panel. In the Playback tab, select your playback device, then in the Properties / Advanced tab <i>uncheck</i> "Allow applications to take exclusive control of this device" Do the same for your microphone.
Also in the Communications Tab  set "When Windows detects communication activity:" Do Nothing

A screen dump of that:
<a target="_blank" href="VoiceRecognition_InstallationTraining.png">
<img src="VoiceRecognition_InstallationTraining.png" alt="Control Panel">
</a>

If it's still not working read the words of wisdom in the <a href="http://thecrewchief.org/showthread.php?28-F-a-q&p=3238&viewfull=1#post3238">FAQ</a>.