CrewChief can display windows from the desktop in VR (SteamVR only).

By default the app will monitor for the SteamVR process, this can be turned off by the Property <b>Enable VR overlays</b>. It is also possible for the app to launch SteamVR when starting the app by enabling <b>Start SteamVR if detected</b> in Properties.

When SteamVR is running the button <b>SteamVR overlay settings</b> will be enabled, and you can control which windows from the desktop you wish to display in VR and control the position / scale / transparency / curvature of the window in the VR world.

The VR setting window will also show up in VR and you can control it using your mouse just like a normal desktop app.

To enable a window in VR select a window from the list box and click <b>Show in VR</b>. With this window selected you can now move it around in VR world with the controls in the settings window.

While it is possible it is currently not recommended to display windows from more than one desktop at a time, you can enable as many windows as you like from one desktop without it having any noticeable performance impact.

The window cannot be minimized or hidden by other windows.